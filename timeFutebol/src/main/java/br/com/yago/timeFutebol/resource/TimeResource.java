package br.com.yago.timeFutebol.resource;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.yago.timeFutebol.model.Time;

@RestController
@CrossOrigin(origins  = "http://localhost:4200")
@RequestMapping(value = "/api/v1")
public class TimeResource {
	
	
	@Autowired
	TimeRepository repository;
	

	@RequestMapping(value = "/times", method = RequestMethod.GET)
	public List<Time> lista_times() {
		return repository.findAll();
	}
	
	@RequestMapping(value = "/times/{id}", method = RequestMethod.GET)
	public ResponseEntity<Time> buscar(@PathVariable("id") Long id) {
	  Time time = repository.findById(id).get();	
	  if (time == null) {
	    return new ResponseEntity<Time>(HttpStatus.NOT_FOUND);
	  }
	  return new ResponseEntity<Time>(time, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/times/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Time> deletar(@PathVariable("id") Long id) {
		
	  Time time = repository.findById(id).get();
	  if (time == null) {
		    return new ResponseEntity<Time>(HttpStatus.NOT_FOUND);
	  }
	  repository.delete(time);	  
	  return new ResponseEntity<Time>(HttpStatus.OK);
	}
	
	@PostMapping("/times")
	public Time incluir(@Valid @RequestBody Time time) {
		return repository.save(time);
	}
	
	@RequestMapping(value = "/times/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> alterarTime(@PathVariable(value = "id") Long id, @Valid @RequestBody Time timeAlterado) {
		Time time = repository.findById(id).get();	
		if (time == null) {
			 return new ResponseEntity<Time>(HttpStatus.NOT_FOUND);
		}
		time.setNome(timeAlterado.getNome());
		time.setCor(timeAlterado.getCor());	  
		repository.save(time);
		return new ResponseEntity<Time>(HttpStatus.OK);
	}

}
