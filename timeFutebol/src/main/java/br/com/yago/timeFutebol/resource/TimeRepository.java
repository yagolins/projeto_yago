package br.com.yago.timeFutebol.resource;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.yago.timeFutebol.model.Time;

@Repository
public interface TimeRepository extends JpaRepository<Time, Long> {
	
}
