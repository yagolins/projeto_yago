import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IncluirTimeComponent } from './incluir-time/incluir-time.component';
import { ListaTimesComponent } from './lista-times/lista-times.component';
import { HttpClientModule } from '@angular/common/http';
import { TimeService } from './time.service';

@NgModule({
  declarations: [
    AppComponent,
    IncluirTimeComponent,
    ListaTimesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule, 
    FormsModule, 
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [TimeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
