import { Observable } from "rxjs";
import { TimeService } from "./../time.service";
import { Time } from "./../time";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: "app-",
  templateUrl: "./lista-times.component.html",
  styleUrls: ["./lista-times.component.css"]
})
export class ListaTimesComponent implements OnInit {
  times: Observable<Time[]>;

  
  
  constructor(
    private timeService: TimeService,
    private route : ActivatedRoute,
    private router : Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.times = this.timeService.lista_times();
    
  }

  deletarTime(id: number) {
    this.timeService.deletarTime(id)
      .subscribe(
        data => {
          this.reloadData();
        },
        error => console.log(error));
  }

  alterar(id:any) {
      this.router.navigate(['alterar', id]);
  }
  
}