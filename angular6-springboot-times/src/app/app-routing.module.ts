import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncluirTimeComponent } from './incluir-time/incluir-time.component';
import { ListaTimesComponent } from './lista-times/lista-times.component';

const routes: Routes = [
  { path: '', redirectTo: 'time', pathMatch: 'full' },
  { path: 'times', component: ListaTimesComponent },
  { path: 'add', component: IncluirTimeComponent },
  { path: 'alterar/:id', component: IncluirTimeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
