import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncluirTimeComponent } from './incluir-time.component';

describe('IncluirTimeComponent', () => {
  let component: IncluirTimeComponent;
  let fixture: ComponentFixture<IncluirTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncluirTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncluirTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
