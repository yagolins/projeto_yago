import { TimeService } from './../time.service';
import { Time } from './../time';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, NgModel } from '@angular/forms';
import { map, switchMap, timeInterval } from 'rxjs/operators';


@Component({
  selector: 'app-incluir-time',
  templateUrl: './incluir-time.component.html',
  styleUrls: ['./incluir-time.component.css']
})
export class IncluirTimeComponent implements OnInit {
  form: FormGroup;
  time: Time = new Time();
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private timeService: TimeService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    let registro = null;
    this.route.params.subscribe(
      (params: any) => {
        const id = params['id'];
        const time$ = this.timeService.getTime(id);

        time$.subscribe(time => {
          registro = time;
          this.updateForm(time);
        });
      }
    );

    this.form = this.fb.group({
      id: [],
      nome: [],
      cor: []
    });

  }

  updateForm(time) {

    this.form.setValue({
      id: time.id,
      cor: time.cor,
      nome: time.nome
    });

  }

  newTime(): void {
    this.submitted = false;
    this.time = new Time();
  }

  save() {
    if (this.form.value.id) {
      this.timeService.alterarTime(this.form.value.id, this.form.value)
        .subscribe(data => console.log(data), error => console.log(error));
    } else {

      this.timeService.incluirTime(this.time)
        .subscribe(data => console.log(data), error => console.log(error));
      this.time = new Time();
    }
  }

  hasError(field: string) {
    return this.form.get(field).errors;
  }

  onSubmit() {
    this.submitted = true;

    this.save();

  }

}
