import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  private baseUrl = 'http://localhost:8080/api/v1/times';

  constructor(private http: HttpClient) { }

  getTime(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  incluirTime(time: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, time);
  }

  alterarTime(id: number, time: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`,time);
  }

  deletarTime(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  lista_times(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
